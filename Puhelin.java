import java.util.ArrayList;
import java.util.Scanner;

public class Puhelin {      
    static Scanner choice = new Scanner(System.in);
    static ArrayList<Information> contactList = new ArrayList<Information>();  

    public static void main(String[] args) {

        boolean quit = false;

        // ----While-loop---- //
        while (!quit) {
            startingText();
            String choose = choice.nextLine();
            switch (choose) {
                case "0":
                    showContacts();
                    break;
                case "1":
                    addContact();
                    break;
                case "2":
                    deleteContact();
                    break;
                case "3":
                    searchContact();
                    break;
                case "4":
                    quit = true;
                    System.out.println("Shutting down....\nShutting down...\nShutting down..\nShutting down.");
                    break;
                default:
                    System.out.println("Invalid command. Try again!");
                    break;
            }
        }
    }

    public static void showContacts() {
        for (Information contact : contactList) {
            System.out.println(contact.toString());
        }
    }

    public static void deleteContact() {        
        System.out.print("Enter the name you want to delete from contacts: ");
        String name = choice.nextLine();
        Information info = new Information("", "");

        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).name.equals(name)) {
                info = contactList.get(i);
                contactList.remove(i);
            }
        }
        if (info.name != "") {
            System.out.println("Deleted Information!");
        } else {
            System.out.println("Cannot find the contact from the list! Please try again!");
        }
    }
    
    public static void addContact() {        
        System.out.print("Enter the name you want to add to contacts: ");
        String name = choice.nextLine();

        System.out.print("Enter the number you want to add to contact " +name + ": ");
        String number = choice.nextLine();
        
        Information info = new Information(name, number);

        contactList.add(info);
    }

    public static void searchContact() {
        //Search for contact
        System.out.print("Enter the name you want to search from contacts: ");
        String name = choice.nextLine();
        Information info = new Information("", "");

        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).name.equals(name)) {
                info = contactList.get(i);
                System.out.println(contactList.get(i));
            }
        }
        if (info.name == "") {
            System.out.println("Cannot find the contact from the list! Please try again!");
        }        
    }

    //Instruction text
    public static void startingText() {
        System.out.println("Select one of the following actions:");
        System.out.println("0. Show all the contacts");
        System.out.println("1. Add new contact");
        System.out.println("2. Delete contact");
        System.out.println("3. Search contact by name");
        System.out.println("4. Quit");
    }
}

