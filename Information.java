public class Information {
    public String name;
    public String number;

	public Information(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public String toString() {
        return this.name + " " + this.number;
    }
}                                                     